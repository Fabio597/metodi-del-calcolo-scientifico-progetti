from datetime import time
import matplotlib.pyplot as plt
import csv
import codecs

import pandas as pd
import plotly.graph_objects as go

def read_memory_data_linux(file, matlab = False):
    data = []
    time = []
    t = 1
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        for row in csv_reader:
            if t%5 == 0:
                if matlab:
                    data.append(float(row[2])/1000)
                else:
                    data.append(float(row[3])/1000)
                time.append(t)
            t += 1
    return (data, time)

def read_memory_data_windows(file, python = False):
    data = []
    time = []
    t = 1
    reader = csv.reader(codecs.open(file, 'rU', 'utf-16'))
    for row in reader:
        if t%5 == 0:
            data.append(row[0])
            time.append(t)
        t += 1
    return (data, time)

data1, time1 = read_memory_data_linux("Linux/Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_Hook_1498.csv", matlab = True)
data2, time2 = read_memory_data_linux("Linux/Python/memory_usage_matrixes/mem_monitor_python_linux_Hook_1498.csv")

data3, time3 = read_memory_data_windows("Windows/MatLab/memory_usage_matrixes/memory_usage_matlab_windows_Hook_1498.txt")
data4, time4 = read_memory_data_windows("Windows/Python/memory_usage_matrixes/memory_usage_python_windows_Hook_1498.txt")


fig = go.Figure()
fig.add_trace(go.Scatter(x=time1, y=data1, mode='lines+markers', name='Matlab - Linux - Hook_1498'))
fig.add_trace(go.Scatter(x=time2, y=data2, mode='lines+markers', name='Python - Linux - Hook_1498'))
fig.add_trace(go.Scatter(x=time3, y=data3, mode='lines+markers', name='Matlab - Windows - Hook_1498'))
fig.add_trace(go.Scatter(x=time4, y=data4, mode='lines+markers', name='Python - Windows - Hook_1498'))
fig.update_layout(yaxis_type="log")
fig.show()
"""

# Memory Objects
# X
x_axis_matlab_windows = [430909, 1679599, 2012833, 3599932, 7660826, 20207907, 28715634, 59374451]
x_axis_python_windows = [430909, 1679599, 2012833, 3599932, 7660826, 20207907, 28715634]
x_axis_matlab_linux = [430909, 1679599, 2012833, 3599932, 7660826, 20207907]
x_axis_python_linux = [430909, 1679599, 2012833, 3599932, 7660826, 20207907, 28715634]
# Y
y_axis_matlab_windows = [1086, 1483, 1233, 859, 3033, 1413, 4766, 5837]
y_axis_python_windows = [127, 405, 162, 716, 2410, 856, 4925, 2002]
y_axis_matlab_linux = [1559, 2093, 1715, 2061, 4070, 2647]
y_axis_python_linux = [140, 423, 175, 742, 1543, 886, 4800]

fig = go.Figure()
fig.add_trace(go.Scatter(x = x_axis_matlab_windows, y = y_axis_matlab_windows, mode='lines+markers', name='Matlab - Windows'))
fig.add_trace(go.Scatter(x = x_axis_python_windows, y = y_axis_python_windows, mode='lines+markers', name='Python - Windows'))
fig.add_trace(go.Scatter(x = x_axis_matlab_linux, y = y_axis_matlab_linux, mode='lines+markers', name='Matlab - Linux'))
fig.add_trace(go.Scatter(x = x_axis_python_linux, y = y_axis_python_linux, mode='lines+markers', name='Python - Linux'))
fig.update_layout(yaxis_type="log")
fig.show()
"""