% Prefix path to load matrixes
prefix = '../matrici/';

% Matrixes used to compute linear systems
matrixes_to_process = ["GT01R.mtx", "TSC_OPF_1047.mtx", "ns3Da.mtx", "nd24k.mtx", "ifiss_mat.mtx", "bundle_adj.mtx", "Hook_1498.mtx", "G3_circuit.mtx"];

% Store in N the number of matrixes to process
matrixes_to_process_sizes = size(matrixes_to_process);
N = matrixes_to_process_sizes(2);

% save cond value
matrixes_cond = zeros(N, 1);

for i = 1:N
    disp(matrixes_to_process(i));
    current_matrix = mmread(prefix + matrixes_to_process(i));
    matrixes_cond(i) = condest(current_matrix);
    disp(matrixes_cond(i));
end

disp(matrixes_cond);