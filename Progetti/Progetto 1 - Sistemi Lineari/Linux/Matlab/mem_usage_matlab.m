% Prefix path to load matrixes
prefix = '../matrici/';

% Matrixes used to compute linear systems
matrixes_to_process = ["GT01R.mtx", "TSC_OPF_1047.mtx", "ns3Da.mtx", "nd24k.mtx", "ifiss_mat.mtx", "bundle_adj.mtx", "Hook_1498.mtx", "G3_circuit.mtx"];

i = 7;

disp(matrixes_to_process(i));
current_matrix = mmread(prefix + matrixes_to_process(i));
current_matrix_size = size(current_matrix);
current_N = current_matrix_size(2);
current_xe = ones(current_N, 1);
current_b = current_matrix*current_xe;
current_xM = current_matrix\current_b;