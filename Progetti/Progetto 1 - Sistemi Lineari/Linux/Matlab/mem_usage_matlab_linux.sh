#!/bin/bash

PROC_NAME=MATLAB
CSV_FILE="./memory_usage_matrixes/mem_monitor_matlab_linux_Hook_1498.csv"

echo "PID,RSS" | tee $CSV_FILE

while :; do
	ps -C $PROC_NAME -o pid=,rss= | tee -a $CSV_FILE
done
