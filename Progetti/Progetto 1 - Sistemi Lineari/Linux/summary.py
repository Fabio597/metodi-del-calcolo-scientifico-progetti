from datetime import time
import matplotlib.pyplot as plt
import csv
import codecs

import pandas as pd
import plotly.graph_objects as go


def read_memory_data(file, matlab = False):
    data = []
    time = []
    t = 1
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        for row in csv_reader:
            print(row)
            if matlab:
                data.append(float(row[2])/1000)
            else:
                data.append(float(row[3])/1000)
            time.append(t)
            t += 1
            
    return (data, time)

"""
data, time = read_memory_data("Python/memory_usage_matrixes/mem_monitor_python_linux_Hook_1498.csv")
#data2, time2 = read_memory_data("Python/memory_usage_matrixes/mem_monitor_python_linux_TSC_OPF_1047.csv")
print(data)
print(min(data))
print(max(data))


fig = go.Figure()
fig.add_trace(go.Scatter(x=time, y=data, mode='lines+markers', name='Matlab - TSC_OPF_1047'))
fig.add_trace(go.Scatter(x=time2, y=data2, mode='lines+markers', name='Python - TSC_OPF_1047'))
fig.update_layout(yaxis_type="log")
fig.show()

data_matlab, time_matlab = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_GT01R.txt")
data_python, time_python = read_memory_data("Python/memory_usage_matrixes/memory_usage_python_windows_GT01R.txt")

fig = go.Figure()
fig.add_trace(go.Scatter(x=time_matlab, y=data_matlab, mode='lines+markers', name='Matlab - GT01R'))
fig.add_trace(go.Scatter(x=time_python, y=data_python, mode='lines+markers', name='Python - GT01R'))
fig.update_layout(yaxis_type="log")
fig.show()

data1, time1 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_bundle_adj.csv", matlab = True)
data2, time2 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_G3_circuit.csv", matlab = True)
data3, time3 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_GT01R.csv", matlab = True)
data4, time4 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_Hook_1498.csv", matlab = True)
data5, time5 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_ifiss_mat.csv", matlab = True)
data6, time6 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_nd24k.csv", matlab = True)
data7, time7 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_ns3Da.csv", matlab = True)
data8, time8 = read_memory_data("Matlab/memory_usage_matrixes/mem_monitor_matlab_linux_TSC_OPF_1047.csv", matlab = True)


fig = go.Figure()
fig.add_trace(go.Scatter(x=time1, y=data1, mode='lines+markers', name='bundle_adj'))
fig.add_trace(go.Scatter(x=time2, y=data2, mode='lines+markers', name='G3_circuit'))
fig.add_trace(go.Scatter(x=time3, y=data3, mode='lines+markers', name='GT01R'))
fig.add_trace(go.Scatter(x=time4, y=data4, mode='lines+markers', name='Hook_1498'))
fig.add_trace(go.Scatter(x=time5, y=data5, mode='lines+markers', name='ifiss_mat'))
fig.add_trace(go.Scatter(x=time6, y=data6, mode='lines+markers', name='nd24k'))
fig.add_trace(go.Scatter(x=time7, y=data7, mode='lines+markers', name='ns3Da'))
fig.add_trace(go.Scatter(x=time8, y=data8, mode='lines+markers', name='TSC_OPF_1047'))
                    
fig.update_layout(yaxis_type="log")
fig.show()
"""
# GT01R, ns3Da, TSC_OPF_1047, ifiss_mat, G3_circuit, bundle_adj, nd24k, Hook_1498
# Hook non c'è per Python -> CholmodError
x_axis_matlab = [430909, 1679599, 2012833, 3599932, 7660826, 20207907]
x_axis_python = [430909, 1679599, 2012833, 3599932, 7660826, 20207907, 28715634]

# Time - Elapsed Windows (Python - Matlab) ------------------------------------------------
# Matlab
y_axis_matlab = [1559, 2093, 1715, 2061, 4070, 2647]
# Python
y_axis_python = [140, 423, 175, 742, 1543, 886, 4800]
#df = pd.DataFrame(list(zip(x_axis_matlab, y_axis_matlab)), columns = ['x', 'y'])

fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis_matlab, y=y_axis_matlab, mode='lines+markers', name='Matlab'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python, mode='lines+markers', name='Python'))
fig.update_layout(yaxis_type="log")
fig.show()

"""
# -------------------------------------------------------------------------------------------
# GT01R, ns3Da, TSC_OPF_1047, ifiss_mat, G3_circuit, bundle_adj, nd24k, Hook_1498
# Error - Windows (Python - Matlab) ------------------------------------------------
y_axis_matlab = [4.43744165823676e-15, 8.74185648902816e-16, 1.66256467498459e-11, 4.73219098464676e-14, 3.57570516918791e-12, 1.12101483800396e-05]
y_axis_python = [6.282977760357075e-13, 8.273151452020635e-15, 2.349998087054139e-11, 3.978676658605639e-14, 1.0, 1.0, 1.0]
fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis_matlab, y=y_axis_matlab,
                    mode='lines+markers',
                    name='Matlab'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python,
                    mode='lines+markers',
                    name='Python'))
fig.update_layout(yaxis_type="log")
fig.show()

# -------------------------------------------------------------------------------------------

# GT01R, ns3Da, TSC_OPF_1047, ifiss_mat, G3_circuit, bundle_adj, nd24k, Hook_1498
# Mmeory Usage - Elapsed Windows (Python - Matlab) ------------------------------------------------
y_axis_matlab = [6.81879425048828, 26.2517471313477, 31.024040222168, 57.8697738647461, 189.472465515137, 331.847648620605]
y_axis_python = [6.787660598754883, 26.097923278808594, 30.96472930908203, 57.136962890625, 141.09512615203857, 316.1871566772461, 439.2690658569336]
fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis_matlab, y=y_axis_matlab,
                    mode='lines+markers',
                    name='Matlab'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python,
                    mode='lines+markers',
                    name='Python'))
fig.update_layout(yaxis_type="log")
fig.show()
# -------------------------------------------------------------------------------------------

file = "MatLab/memory_usage_matrixes/memory_usage_matlab_windows_Hook_1498.csv"
mem, time = read_data(file)
print(len(mem))
print(len(time))


plt.semilogy(time, mem, color = 'b', linewidth = 1, label= 'Matlab')
plt.xlabel('Time')
plt.ylabel('Memory')
plt.title('Dimension - Memory')
plt.legend()
plt.show()
"""