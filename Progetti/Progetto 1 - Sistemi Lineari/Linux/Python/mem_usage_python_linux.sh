#!/bin/bash

PROC_NAME=python3
CSV_FILE="./memory_usage_matrixes/mem_monitor_python_linux_nd24k.csv"

echo "PID,RSS" | tee $CSV_FILE

while :; do
	ps -C $PROC_NAME -o pid=,rss= | tee -a $CSV_FILE
done
