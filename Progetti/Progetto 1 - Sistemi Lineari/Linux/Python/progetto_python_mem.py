import scipy.sparse.linalg as sp
from scipy.io import mmread
import numpy as np
from sksparse.cholmod import cholesky

prefix = '../matrici/'
files = ['GT01R.mtx', 'TSC_OPF_1047.mtx', 'ns3Da.mtx', 'nd24k.mtx', 'ifiss_mat.mtx', 'bundle_adj.mtx', 'Hook_1498.mtx', 'G3_circuit.mtx']
positive_defined = [False, False, False, True, False, True, True, True]

def memory_usage(prefix, files, positive_defined, i):
	print(files[i])
	current_matrix = mmread(prefix + files[i])
	print("matrice letta")
	print(current_matrix)
	current_size = current_matrix.shape[0]
	current_xe = np.ones((current_size,), dtype=int)
	current_b = current_matrix.dot(current_xe)
	print("b calcolato")
	if not(positive_defined[i]):
		sp.spsolve(current_matrix.tocsc(), current_b)
	else:
		print("cholesky")
		factor = cholesky(current_matrix.tocsc())
		print("cholesky calcolato")
		factor(current_b)	

memory_usage(prefix, files, positive_defined, 3)

