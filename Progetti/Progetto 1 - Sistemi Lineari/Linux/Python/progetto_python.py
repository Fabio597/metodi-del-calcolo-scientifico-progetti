from sksparse.cholmod import cholesky
import scipy.sparse.linalg as sp
from scipy.io import mmread
import matplotlib.pyplot as plt
import numpy as np
import time
import tracemalloc
import pandas as pd 


prefix = '../matrici/'
files = ['GT01R.mtx', 'TSC_OPF_1047.mtx', 'ns3Da.mtx', 'nd24k.mtx', 'ifiss_mat.mtx', 'bundle_adj.mtx', 'Hook_1498.mtx', 'G3_circuit.mtx']
positive_defined = [False, False, False, True, False, True, True, True];

time_spent = []
dimensions = []
error = []
memory_usage = []
# for Hook_1498 -> sksparse.cholmod.CholmodOutOfMemoryError
for i in range(0, len(files)):
	tracemalloc.start()
	print(files[i])
	current_matrix = mmread(prefix + files[i])
	current_size = current_matrix.shape[0]
	current_xe = np.ones((current_size,), dtype=int)
	current_b = current_matrix.dot(current_xe)

	current_xM = 0
	t = time.time()
	try:
		if positive_defined[i]:	
			factor = cholesky(current_matrix.tocsc())
			curret_xM = factor(current_b)
		else:
			# solve = sp.factorized(current_matrix) # Makes LU decomposition.
            # current_xM = solve(current_b) # Uses the LU factors.
			current_xM = sp.spsolve(current_matrix.tocsc(), current_b)
	except:
		continue
	elapsed = time.time() - t
	current, peak = tracemalloc.get_traced_memory()
	tracemalloc.stop()
	dimensions.append(current_size)
		
	numerator = np.linalg.norm(current_xM - current_xe)
	denominator = np.linalg.norm(current_xe)
	error.append(numerator/denominator)
	time_spent.append(elapsed)
	memory_usage.append(current/1048576)
		
		
    	
print(dimensions)
print(error)
print(time_spent)
print(memory_usage)

pd.DataFrame(dimensions).to_csv("results/dim_results.csv")
pd.DataFrame(time_spent).to_csv("results/time_results.csv")
pd.DataFrame(error).to_csv("results/error_results.csv")
pd.DataFrame(memory_usage).to_csv("results/memuse_results.csv")

x_axis = dimensions

# dimensions - time elapsed
y_axis = time_spent
plt.semilogy(x_axis, y_axis, color = 'k', marker = 'o', linewidth = 2, markersize = 10)
plt.xlabel('Dimension')
plt.ylabel('Time Elapsed')
plt.title('dimensions - time elapsed')
plt.show()

# dimensions - relative error
y_axis = error
plt.semilogy(x_axis, y_axis, color = 'k', marker = 'o', linewidth = 2, markersize = 10)
plt.xlabel('Dimension')
plt.ylabel('Relative Error')
plt.title('dimensions - relative error')
plt.show()

# dimensions - memory usage
y_axis = memory_usage
plt.semilogy(x_axis, y_axis, color = 'k', marker = 'o', linewidth = 2, markersize = 10)
plt.xlabel('Dimension')
plt.ylabel('Memory Usage')
plt.title('dimensions - memory usage')
plt.show()
