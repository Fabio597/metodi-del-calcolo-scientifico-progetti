$text = '';
while($true) {
	try {
		$r = Get-Process Python -ErrorAction Stop | Select-Object Name,@{Name='WorkingSet';Expression={($_.WorkingSet64/1MB)}}
		$r.WorkingSet >> "./memory_usage_matrixes/memory_usage_python_windows_G3_circuit.txt"
		$text = $text+"`n"+$r
	}
	catch {
		Write-Host "Error process not found!"
		$bol = 1;
	}
}
