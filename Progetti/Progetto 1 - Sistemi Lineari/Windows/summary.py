from datetime import time
import matplotlib.pyplot as plt
import csv
import codecs

import pandas as pd
import plotly.graph_objects as go

"""
def read_memory_data(file, python = False):
    data = []
    time = []
    t = 1
    reader = csv.reader(codecs.open(file, 'rU', 'utf-16'))
    for row in reader:
        data.append(int(row[0]))
        time.append(t)
        t += 1
    return (data, time)

data, time = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_GT01R.txt")
print(data)
print(min(data))
print(max(data))

data_matlab, time_matlab = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_GT01R.txt")
data_python, time_python = read_memory_data("Python/memory_usage_matrixes/memory_usage_python_windows_GT01R.txt")

fig = go.Figure()
fig.add_trace(go.Scatter(x=time_matlab, y=data_matlab, mode='lines+markers', name='Matlab - GT01R'))
fig.add_trace(go.Scatter(x=time_python, y=data_python, mode='lines+markers', name='Python - GT01R'))
fig.update_layout(yaxis_type="log")
fig.show()


data1, time1 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_bundle_adj.txt")
data2, time2 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_G3_circuit.txt")
data3, time3 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_GT01R.txt")
data4, time4 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_Hook_1498.txt")
data5, time5 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_ifiss_mat.txt")
data6, time6 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_nd24k.txt")
data7, time7 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_ns3Da.txt")
data8, time8 = read_memory_data("MatLab/memory_usage_matrixes/memory_usage_matlab_windows_TSC_OPF_1047.txt")


fig = go.Figure()
fig.add_trace(go.Scatter(x=time1, y=data1, mode='lines+markers', name='bundle_adj'))
fig.add_trace(go.Scatter(x=time2, y=data2, mode='lines+markers', name='G3_circuit'))
fig.add_trace(go.Scatter(x=time3, y=data3, mode='lines+markers', name='GT01R'))
fig.add_trace(go.Scatter(x=time4, y=data4, mode='lines+markers', name='Hook_1498'))
fig.add_trace(go.Scatter(x=time5, y=data5, mode='lines+markers', name='ifiss_mat'))
fig.add_trace(go.Scatter(x=time6, y=data6, mode='lines+markers', name='nd24k'))
fig.add_trace(go.Scatter(x=time7, y=data7, mode='lines+markers', name='ns3Da'))
fig.add_trace(go.Scatter(x=time8, y=data8, mode='lines+markers', name='TSC_OPF_1047'))
                    
fig.update_layout(yaxis_type="log")
fig.show()


"""
# ["GT01R.mtx", "TSC_OPF_1047.mtx", "ns3Da.mtx", "nd24k.mtx", "ifiss_mat.mtx", "bundle_adj.mtx", "Hook_1498.mtx", "G3_circuit.mtx"];
# GT01R, ns3Da, TSC_OPF_1047, ifiss_mat, G3_circuit, bundle_adj, nd24k, Hook_1498
x_axis_matlab = [430909, 1679599, 2012833, 3599932, 7660826, 20207907, 28715634, 59374451]
x_axis_python = [430909, 1679599, 2012833, 3599932, 7660826, 20207907, 28715634]
"""
# Time - Elapsed Windows (Python - Matlab) ------------------------------------------------
# Matlab
y_axis_matlab = [76, 430, 204, 859, 471, 2380, 886, 4645, 5828]

# Python
y_axis_python = [126, 404, 161, 715, 2409, 855, 4924, 2000]
#df = pd.DataFrame(list(zip(x_axis_matlab, y_axis_matlab)), columns = ['x', 'y'])

fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis_matlab, y=y_axis_matlab, mode='lines+markers', name='Matlab'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python, mode='lines+markers', name='Python'))
fig.update_layout(yaxis_type="log")
fig.show()

# -------------------------------------------------------------------------------------------
"""
# GT01R, ns3Da, TSC_OPF_1047, ifiss_mat, G3_circuit, bundle_adj, nd24k, Hook_1498
# Error - Windows (Python - Matlab) ------------------------------------------------
y_axis_matlab_windows = [4.43744165823676e-15, 8.74185648902816e-16, 1.2416780211217e-11, 4.73219098464676e-14, 3.57570516918791e-12, 1.12101483800396e-05, 3.85355897993362e-11, 9.19942926257956e-14]
y_axis_python_windows = [5.542196443840117e-13, 8.489863961655762e-15, 1.666967199914578e-11, 5.356496738211887e-14, 3.57573880e-12, 1.42223060e-05, 3.92907669e-11]
y_axis_matlab_linux = [4.4374416e-15, 8.7418564e-16, 1.6625646e-11,4.7321909e-14, 3.5757051e-12, 1.1210148e-05]
y_axis_python_linux = [6.282977e-13, 8.273151e-15, 2.349998e-11, 3.978676e-14, 3.6637398e-12, 1.32264230e-05, 3.9290766e-11]
fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis_matlab, y=y_axis_matlab_windows, mode='lines+markers', name='Matlab - Windows'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python_windows, mode='lines+markers', name='Python - Windows'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_matlab_linux, mode='lines+markers', name='Matlab - Linux'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python_linux, mode='lines+markers', name='Python - Linux'))
fig.update_layout(yaxis_type="log")
fig.show()
"""
# -------------------------------------------------------------------------------------------

# Mmeory Usage - Elapsed Windows (Python - Matlab) ------------------------------------------------
# GT01R, ns3Da, TSC_OPF_1047, ifiss_mat, G3_circuit, bundle_adj, nd24k, Hook_1498
y_axis_matlab = [6.81879425048828, 26.2517471313477, 31.024040222168, 57.8697738647461, 189.472465515137, 331.847648620605, 441.461814880371, 974.55638885498]
y_axis_python = [6.787660598754883, 26.097923278808594, 30.96472930908203, 56.76958084106445, 135.0470609664917, 314.22887802124023, 438.9944076538086]
fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis_matlab, y=y_axis_matlab,
                    mode='lines+markers',
                    name='Matlab'))
fig.add_trace(go.Scatter(x=x_axis_python, y=y_axis_python,
                    mode='lines+markers',
                    name='Python'))
fig.update_layout(yaxis_type="log")
fig.show()
# -------------------------------------------------------------------------------------------

file = "MatLab/memory_usage_matrixes/memory_usage_matlab_windows_Hook_1498.csv"
mem, time = read_data(file)
print(len(mem))
print(len(time))


plt.semilogy(time, mem, color = 'b', linewidth = 1, label= 'Matlab')
plt.xlabel('Time')
plt.ylabel('Memory')
plt.title('Dimension - Memory')
plt.legend()
plt.show()
"""