$text = '';
while($true) {
	try {
		$r = Get-Process MATLAB -ErrorAction Stop | Select-Object Name,@{Name='WorkingSet';Expression={($_.WorkingSet64/1MB)}}
		$r.WorkingSet >> "./memory_usage_matrixes/memory_usage_matlab_windows_G3_circuit.txt"
		$text = $text+"`n"+$r
	}
	catch {
		Write-Host "Process not yet present"
	}
}
