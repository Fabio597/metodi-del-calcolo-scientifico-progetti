% Prefix path to load matrixes
prefix = '../matrici/';

% Matrixes used to compute linear systems
matrixes_to_process = ["GT01R.mtx", "TSC_OPF_1047.mtx", "ns3Da.mtx", "nd24k.mtx", "ifiss_mat.mtx", "bundle_adj.mtx", "Hook_1498.mtx", "G3_circuit.mtx"];

% Store in N the number of matrixes to process
matrixes_to_process_sizes = size(matrixes_to_process);
N = matrixes_to_process_sizes(2);

% Array used to store dimension, time, error
dimensions = zeros(N, 1);
time = zeros(N, 1);
error = zeros(N, 1);
memory_usage = zeros(N, 1);

for i = 1:N
    % variables used to calculate the linear system
    disp(matrixes_to_process(i));
    [userview1,systemview1] = memory;
    disp("memory used before " + userview1.MemUsedMATLAB/2^20 +" MB")
    disp("memory available before " + userview1.MemAvailableAllArrays/2^20+" MB")
    current_matrix = mmread(prefix + matrixes_to_process(i));
    current_matrix_size = size(current_matrix);
    current_N = current_matrix_size(2);
    current_xe = ones(current_N, 1);
    current_b = current_matrix*current_xe;

    % Calculate the linear system and save parameters
    dimensions(i) = current_N;
    tstart = tic; current_xM = current_matrix\current_b; time(i) = toc(tstart);
    error(i) = norm(current_xM - current_xe)/norm(current_xe);
    [userview2,systemview2] = memory;
    disp("memory used after " + userview2.MemUsedMATLAB/2^20+" MB")
    disp("memory available after " + userview2.MemAvailableAllArrays/2^20+" MB")
    memory_usage(i) = monitor_memory_whos;
end

disp("time");
disp(time);
disp("error");
disp(error);
disp("dimensions");
disp(dimensions);
disp("memory usage");
disp(memory_usage);

results_prefix = '../results.csv';
writematrix(dimensions,'results/dim_results.csv','WriteMode','overwrite')
writematrix(time,'results/time_results.csv','WriteMode','overwrite')
writematrix(error,'results/error_results.csv','WriteMode','overwrite')
writematrix(memory_usage,'results/memuse_results.csv','WriteMode','overwrite')

figure()
semilogy(dimensions, time, 'LineStyle', '--', 'Marker','square', 'Linewidth', 2, 'MarkerSize', 12, 'Color', 'b');
xlim([min(dimensions) max(dimensions)])
ylim([min(time) max(time)])
xlabel('\texttt{Dimension}', 'Interpreter', 'latex', 'FontSize', 20);
ylabel('\texttt{Time Elapsed}', 'Interpreter', 'latex', 'FontSize', 20);
hold on

figure()
semilogy(dimensions, error, 'LineStyle', '--', 'Marker','square', 'Linewidth', 2, 'MarkerSize', 12, 'Color', 'b');
xlim([min(dimensions) max(dimensions)])
ylim([min(error) max(error)])
xlabel('\texttt{Dimension}', 'Interpreter', 'latex', 'FontSize', 20);
ylabel('\texttt{Relative Error}', 'Interpreter', 'latex', 'FontSize', 20);
hold on

figure()
semilogy(dimensions, memory_usage, 'LineStyle', '--', 'Marker','square', 'Linewidth', 2, 'MarkerSize', 12, 'Color', 'b');
xlim([min(dimensions) max(dimensions)])
ylim([min(memory_usage) max(memory_usage)])
xlabel('\texttt{Dimension}', 'Interpreter', 'latex', 'FontSize', 20);
ylabel('\texttt{Memory Usage}', 'Interpreter', 'latex', 'FontSize', 20);