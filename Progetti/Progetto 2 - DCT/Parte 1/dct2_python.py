from my_dct2 import my_dct, my_idct, my_dct2, my_idct2
from create_data import generate_matrixes
import numpy as np
import scipy.fftpack
import time
import plotly.graph_objects as go

def dct(a):
	return scipy.fftpack.dct(a, norm='ortho')

def idct(a):
	return scipy.fftpack.idct(a, norm='ortho')

def dct2(a):
	return dct(np.transpose(dct(np.transpose(a))))

def idct2(a):
	return idct(np.transpose(idct(np.transpose(a))))

a1 = [ 231, 32, 233, 161, 24, 71, 140, 245 ]

a2 = [
		[ 231, 32, 233, 161, 24, 71, 140, 245 ],
		[ 247, 40, 248, 245, 124, 204, 36, 107 ],
		[ 234, 202, 245, 167, 9, 217, 239, 173 ],
		[ 193, 190, 100, 167, 43, 180, 8, 70 ],
		[ 11, 24, 210, 177, 81, 243, 8, 112 ],
		[ 97, 195, 203, 47, 125, 114, 165, 181 ],
		[ 193, 70, 174, 167, 41, 30, 127, 245 ],
		[ 87, 149, 57, 192, 65, 129, 178, 228 ]
	]

#print(dct(a1))
#print(my_dct(np.transpose(a2)))
#print(my_dct(a1))
#print(idct2(dct2(a2)))
#print(my_idct2(my_dct2(a2)))
#print(dct(a1))
#print(my_dct(a1))
"""
matrixes_to_process = generate_matrixes()

dimensions = []
time_dct2_scipy = []
time_dct2_my = []

for matrix in matrixes_to_process:
	dimensions.append(len(matrix))

	# dct2 - scipy
	start_dct2_scipy = time.time()
	dct2(matrix)
	end_dct2_scipy = time.time() - start_dct2_scipy
	print("dct2 done", len(matrix))

	# dct2 - my algorithm
	start_dct2_my = time.time()
	my_dct2(matrix)
	end_dct2_my = time.time() - start_dct2_my
	print("my dct2 done", len(matrix))

	time_dct2_scipy.append(end_dct2_scipy)
	time_dct2_my.append(end_dct2_my)

print(dimensions)
print(time_dct2_scipy)
print(time_dct2_my)


x_axis = dimensions

y_axis1 = time_dct2_scipy
y_axis2 = time_dct2_my

fig = go.Figure()
fig.add_trace(go.Scatter(x=x_axis, y=y_axis1, mode='lines+markers', name='dct2 (FFT) - Scipy'))
fig.add_trace(go.Scatter(x=x_axis, y=y_axis2, mode='lines+markers', name='My dct2'))
fig.update_layout(yaxis_type="log")
fig.show()
"""