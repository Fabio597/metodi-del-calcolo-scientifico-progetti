import numpy as np

def create_rand_matrix(n, min = 0, max = 255):
    matrix = np.random.uniform(low=min, high=max, size=(n,n))
    for row_index, row in enumerate(matrix):
        for column_index, value in enumerate(row):
            matrix[row_index][column_index] = round(value)
    return matrix

# 2800
def generate_matrixes(min = 200, number_of_matrixes = 3100, hop = 100):
    matrixes = []
    for i in range(min, number_of_matrixes, hop):
        matrixes.append(create_rand_matrix(i))
    return matrixes