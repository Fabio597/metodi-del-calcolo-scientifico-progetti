import numpy as np
import math

# My DCT -> O(N^2)
def my_dct(m_array):
	# get length of array
	n = len(m_array)
	# initialize array of n zeros
	c_array = [0]*n
	# calculate alfa_k for:
	# - k = 0 -> 1/sqrt(n)
	# - k > 0 -> 1/sqrt(n/2)
	alfa_0 = 1/math.sqrt(n)
	alfa_k = 1/math.sqrt(n/2)
	# calculate every Ck
	for k in range(0, n):
		sum = 0
		# calculate sum of Vj * cos(k*pi*2j - 1/2n)
		# range is from 0 to n -> j = j + 1 -> 2(j + 1) - 1 = 2j + 1
		for j in range(0, n):
			beta = k*math.pi*((2*j + 1)/(2*n))
			sum = sum + (math.cos(beta) * m_array[j])
		# multiply by correct alfa
		if k == 0:
			c_array[k] = alfa_0 * sum
		else:
			c_array[k] = alfa_k * sum
	# return dct array
	return c_array

# My IDCT -> O(N^2)
def my_idct(c_array):
	# get length of array
	n = len(c_array)
	# initialize array of n zeros
	m_array = [0]*n
	# calculate alfa_k for:
	# - k = 0 -> 1/sqrt(n)
	# - k > 0 -> 1/sqrt(n/2)
	alfa_0 = 1/math.sqrt(n)
	alfa_k = 1/math.sqrt(n/2)
	# calculate every Vj
	for j in range(0, n):
		sum = 0
		# calculate sum of Ck * cos(k*pi*2j - 1/2n)
		# range is from 0 to n -> j = j + 1 -> 2(j + 1) - 1 = 2j + 1
		for k in range(0, n):
			beta = k*math.pi*((2*j + 1)/(2*n))
			# multiply by correct alfa
			if k == 0:
				sum = sum + (alfa_0 * c_array[k]*math.cos(beta))
			else:
				sum = sum + (alfa_k * c_array[k]*math.cos(beta))
		# execute round to keep only integer part
		m_array[j] = sum
	# return idct array
	return m_array

# My DCT2 -> Execute DCT by rows and then by columns
# O(N^3)
def my_dct2(m_matrix):
	return my_dct(np.transpose(my_dct(np.transpose(m_matrix))))

# My IDCT2 -> Execute DCT by rows and then by columns
# # O(N^3)
def my_idct2(c_matrix):
	return my_idct(np.transpose(my_idct(np.transpose(c_matrix))))