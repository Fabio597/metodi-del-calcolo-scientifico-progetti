import tkinter as tk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.image import imread
from image_cut import image_cut

def browse_image():
	filePath = tk.filedialog.askopenfilename()
	if (filePath):
		image_url.set(filePath)

def run_image_cut():
	imPath = image_url.get()
	
	f = int(entry_F.get())
	d = int(entry_d.get())

	if d > ((2*f) - 2):
		d = ((2*f) - 2)
	
	im1 = imread(imPath)

	# if matrix has 3 dimensions, delete the last one
	if (im1.ndim >= 3):
		im1 = im1[:,:,0]
	
	# apply algorithm
	im2 = image_cut(im1, f, d)
	
	# show images (orginal image and processed image)
	begin_image.imshow(im1, cmap='gray')
	after_image.imshow(im2, cmap='gray')
	
	canvas.draw()

# create window for GUI
window = tk.Tk()
# set title
window.wm_title('Parte 2.2 - Metodi del Calcolo Scientifico')

# Frame to embed buttons, labels, entries
frame_input = tk.Frame(master = window)
frame_input.pack(side = tk.TOP)

# Label near image url selected 
label_near_image_url = tk.Label(master=frame_input, text='Immagine Selezionata:')
label_near_image_url.pack(side = tk.LEFT)

# String URL of choosen image
image_url = tk.StringVar()
image_url.set('nessun url')
# Label of URL String
label_image_url = tk.Label(master = frame_input, textvariable=image_url)
label_image_url.pack(side=tk.LEFT)

# Browse Button to open image chooser
button_browse = tk.Button(master = frame_input, text='Sfoglia Immagini...', command = browse_image)
button_browse.pack(side=tk.LEFT)
tk.Label(master=frame_input, text='     ').pack(side=tk.LEFT)
# Label for F input
label_F = tk.Label(master=frame_input, text='F =')
label_F.pack(side=tk.LEFT)

# Entry to write F value by user
entry_F = tk.Entry(master=frame_input)
# default value for F is 10
entry_F.insert(0, '10')
entry_F.pack(side=tk.LEFT)

# Label for d input
label_d = tk.Label(master = frame_input, text='d =')
label_d.pack(side=tk.LEFT)

# Entry to write d value by user
entry_d = tk.Entry(master=frame_input)
# default value for F is 7
entry_d.insert(0, '7')
entry_d.pack(side=tk.LEFT)
tk.Label(master = frame_input, text='     ').pack(side=tk.LEFT)
# Button apply to execute image processing
button_apply = tk.Button(master = frame_input, text='Esegui', command = run_image_cut)
button_apply.pack(side=tk.LEFT)

# Show Images

# Create Figure object
figure = Figure()
# add_subplot(number of rows, number of columns, index)
begin_image = figure.add_subplot(1,2,1)
# Set title
begin_image.set_title('Immagine Iniziale')
after_image = figure.add_subplot(1,2,2)
after_image.set_title('Immagine Finale')

# Embedding Figure in tk
# https://matplotlib.org/stable/gallery/user_interfaces/embedding_in_tk_sgskip.html
canvas = FigureCanvasTkAgg(figure, master=window)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)

toolbar = NavigationToolbar2Tk(canvas, window)
toolbar.update()
canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)

# fuction that start the window
window.mainloop()