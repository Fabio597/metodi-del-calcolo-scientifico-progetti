import scipy.fftpack
import numpy as np

def dct(a):
	return scipy.fftpack.dct(a, norm='ortho')

def idct(a):
	return scipy.fftpack.idct(a, norm='ortho')

def dct2(a):
	return dct(np.transpose(dct(np.transpose(a))))

def idct2(a):
	return idct(np.transpose(idct(np.transpose(a))))

def image_cut(matrix, F, d):
    final_matrix = matrix.copy()

    height = final_matrix.shape[0]
    weight = final_matrix.shape[1]

    print("begin")
    # x è nel range (0, height) e il ciclo for
    # effettua dei salti di ampiezza F
    for x in range(0, height, F):
        # come sopra effettuiamo salti di ampiezza F
        # così da avere blocchi FxF
        for y in range(0, weight, F):
            # salvo il blocco
            block = final_matrix[x:x+F,y:y+F]
            # applico la dct2 di scipy al blocco
            block = dct2(block)

            block_height = block.shape[0]
            block_weight = block.shape[1]

            # metto i valori in positione (i + j) >= d a zero
            # per eliminarli
            for i in range(0, block_height):
                for j in range(0, block_weight):
                    if (i + j >= d):
                        block[i, j] = 0
            
            # applico la idct2 di scipy al blocco
            block = idct2(block)

            # effettuo l'arrotondamento e:
            # se un valore è maggiore di 255 -> 255
            # se è minore di 0 -> 0
            for i in range(0, block_height):
                for j in range(0, block_weight):
                    block[i,j] = round(block[i,j])
                    if (block[i,j] < 0):
                        block[i, j] = 0
                    if (block[i,j] > 255):
                        block[i, j] = 255

            final_matrix[x:x+F,y:y+F] = block
    
    print("End")
    return final_matrix
